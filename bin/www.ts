import * as app from '../app';
import * as http from 'http';
import * as debug from 'debug';

let log = debug('modern-express:server');
log.log = console.log.bind(console);

let PORT = process.env.PORT || '3000';

app.set('port', PORT);

const server = http.createServer(app);

server.listen(PORT);

server.on('error', (error) => {
    console.error(JSON.stringify(error));
    throw error;
});

server.on('listening', () => {
    const addr = server.address();
    const bind = (typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`);
    log(`Listening on ${bind}`);
});
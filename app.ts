import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as routes from './routes/user-api';
import * as mongoose from 'mongoose';

const app = express();


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));


app.use('/', routes);

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    let err: any = new Error('Not Found');
    err.status = 404;
    next(err);
});


app.use(function(err: any, req: express.Request, res: express.Response, next: Function) {
    res.status(err.status || 500);
    res.json( {
        message: err.message,
        error: {}
    });
});

const MONGODB_CONNECTION: string = "mongodb://localhost:27017/local";

mongoose.connect(MONGODB_CONNECTION, (err) => {
    if (err) {
        console.log(err.message);
        console.log(err);
    }
    else {
        console.log('Connected to MongoDb');
    }
});

export = app;
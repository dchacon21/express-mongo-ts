import * as mongoose from 'mongoose';

export interface UserModel extends mongoose.Document {
    email: string;
    firstName: string;
    lastName: string;
    createdAt: Date;
    modifiedAt: Date;
}

let schema = new mongoose.Schema({
    email: {
        type: String, required: true
    },
    firstName: {
        type: String, required: true
    },
    lastName: {
        type: String, required: false
    },
    createdAt: {
        type: Date, required: false
    },
    modifiedAt: {
        type: Date, required: false
    }
}).pre('save', function(next) {
    if (this._doc) {
        let doc = <UserModel>this._doc;
        let now = new Date();
        if (!doc.createdAt) {
            doc.createdAt = now;
        }
        doc.modifiedAt = now;
    }
    next();
    return this;
});


export let UserRepository = mongoose.model<UserModel>('user', schema, 'users', true);
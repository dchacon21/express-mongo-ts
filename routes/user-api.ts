import * as express from 'express';
import {UserRepository, UserModel} from "../domain/user";


const router = express.Router();


router.get('/users', (req: express.Request, resp: express.Response, next: express.NextFunction) => {
    if(req.query.firstName){
        UserRepository.find({firstName: req.query.firstName},(err, res) => handle(resp, err, res));
    }else {
        UserRepository.find((err, res) => handle(resp, err, res));
    }
});

router.get('/users/:id', (req: express.Request, resp: express.Response, next: express.NextFunction) => {
    UserRepository.findById(req.params.id,(err, res: UserModel) => handle(resp, err, res));
});

router.post('/users', (req: express.Request, resp: express.Response, next: express.NextFunction) => {
    let model: UserModel = req.body;
    UserRepository.create(model, (err, res) => handle(resp, err, res));
});

function handle(response: express.Response, err, res){
    if (err) {
        console.error(err);
        response.json(500, {"error": err});
    }
    else {
        response.json(res);
    }
}

export = router;